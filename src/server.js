const express = require('express');
const path = require('path');
const app = express();
// settings
// definimos el puerto en la variable port
app.set('port', process.env.PORT || 8080)

// definimos la ruta de la vista para que node la encuentre
// dirname da la ruta de la carpeta no importa el sistema
// path permite usar slash o backslash no importa el sistema
app.set('views', path.join(__dirname, 'views'))

// Middlewares 
app.use(express.urlencoded({extended: false}));
// Routes
app.get('/', function(req, res){
    res.send('Hello world')
})

// Static Files
// es para hacer accesibles los archivos que estén en la ruta
app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;
